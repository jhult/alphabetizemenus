﻿# AlphabetizeMenus

## Component Information
* Created By: Core Content Only
* Authors: Jason Stortz and Jonathan Hult
* Last Updated: build_7_20140808

## Overview
This component alphabetizes the menus in WebCenter Content.

![Without component](http://www.redstonecontentsolutions.com/uploads/1/4/3/5/14350456/497757_orig.png)
![With component](http://www.redstonecontentsolutions.com/uploads/1/4/3/5/14350456/6418999_orig.png)

Detailed information on how this component works can be found here:
<http://www.redstonecontentsolutions.com/5/post/2011/09/alphabetizemenus-11g.html>

## Compatibility
This component was built upon and tested on the version listed below, this is the only version it is known to work on, but it may work on older/newer versions.

* 11gR1-11.1.1.7.0-idcprod1-130304T092605 (Build: 7.3.4.184)
* 10.1.3.5.1 (111229) (Build: 7.2.4.105)

## Changelog
* build_7_20140808
	- Fixed issue with sortData[k] being undefined in URM
* build_6_20130620
	- Fixed undefined errors
* build_5_20110913
	- Fixed Trays layout sorting in 11g
* build_4_20110712
	- Added compatibility with 11g
* build_3_20100521
	- Fixed sorting for Safari and Chrome
	- Thanks to Shane D. for helping with the testing!
* build_2_20091119
	- Added preference variable to control whether or not sorting is on/off. This allows for enabling/disabling the component functionality without a  Content Server restart
* build_1_20081129
	- Initial component release